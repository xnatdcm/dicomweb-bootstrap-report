package org.nrg.dicomweb.bootstrap.reporter;

import org.nrg.xnatClient.XNATClient;

import java.io.PrintStream;
import java.util.*;
import java.util.stream.Collectors;

public class ReporterDupStudyUIDs implements Reporter {
    private XNATClient xnatClient;

    public ReporterDupStudyUIDs( XNATClient xnatClient) {
        this.xnatClient = xnatClient;
    }

    private void reportTitle( PrintStream ps) {
        ps.println("Same Studies in Multiple Projects");
    }

    private void reportNumberOfStudies( int nStudies, PrintStream ps) {
        ps.println("Number of studies: " + nStudies);
    }

    private void reportNumberOfProjects( int nProjects, PrintStream ps) {
        ps.println("Number of studies: " + nProjects);
    }

    private static String recordFormat = "%s,%s,%s,%s";

    public void reportHeader( PrintStream ps) {
        ps.println( String.format(recordFormat, "Project Label", "SessionID", "Session Label", "Study Instance UID"));
    }

    public void reportRecord( String projectLabel, String sessionID, String sessionLabel, String studyUID, PrintStream ps) {
        ps.println( String.format( recordFormat, projectLabel, sessionID, sessionLabel, studyUID));
    }

    public void report( PrintStream ps) {
        reportTitle( ps);
        ps.println();

        Map<String, Integer> studyUIDs = xnatClient.getProjectCountPerStudyInstanceUID();

        List<String> dupStudyUIDs = studyUIDs.keySet().stream()
                .filter( suid -> studyUIDs.get( suid) > 2)
                .collect(Collectors.toList());

        reportNumberOfStudies( dupStudyUIDs.size(), ps);
        ps.println();


//        ps.println("Projects with duplicate Study InstanceUIDs.");
//        ps.println();
//        ps.println("Project,Conflicting Project");
//        Set<String> set = new HashSet<>();
//        Set<Set<String>> setset = new HashSet<>();
//
//        List<String> projectLabels = xnatClient.getProjectLabels();
//        set.addAll( projectLabels);
//        for( String studyUID: dupStudyUIDs) {
//            setset.add( new HashSet<>( xnatClient.getProjectLabelsByStudyInstanceUID(studyUID)));
//        }
//        Map<String, Set<String>> partition = partition( projectLabels, setset);
//        for( String pLabel: partition.keySet()) {
//            ps.println(pLabel + ",");
//            partition.get( pLabel).stream().forEachOrdered( p-> ps.println("," + p));
//        }

        reportHeader( ps);
        for( String studyUID: dupStudyUIDs) {
            List<String> projectLabels = xnatClient.getProjectLabelsByStudyInstanceUID( studyUID);
            boolean first = true;
            for( String projectLabel: projectLabels) {
                String uid = (first)? studyUID: "";
                Properties sessionIdentifiers = xnatClient.getSesssionIdentifiers( projectLabel, studyUID);
                reportRecord( projectLabel, sessionIdentifiers.getProperty("sessionid"), sessionIdentifiers.getProperty("sessionlabel"), uid, ps);
                first = false;
            }
        }
    }

    public Set<String> subset( String element, Set<Set<String>> setset) {
        Set<String> subset = new HashSet<>();
        setset.forEach( set -> {
            if( set.contains( element)) {
                subset.addAll( set);
                subset.remove( element);
            }
        });
        return subset;
    }

    public Map<String, Set<String>> partition( List<String> elements, Set<Set<String>> setset) {
        return elements.stream()
                .collect(Collectors.toMap( e -> e, e -> subset( e, setset)));
    }


    @Override
    public void report(String projectLabel, String subjectLabel, PrintStream ps) {
        report( ps);
    }
}
