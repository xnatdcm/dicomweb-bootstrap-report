package org.nrg.dicomweb.bootstrap;

import lombok.extern.slf4j.Slf4j;
import org.nrg.dicomweb.bootstrap.reporter.Reporter;
import org.nrg.dicomweb.bootstrap.reporter.ReporterDBvsDICOM;
import org.nrg.xnatClient.XNATClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.stream.Collectors;

@SpringBootApplication
@PropertySource("xnat.properties")
@Slf4j
public class Application {
    @Autowired
    private XNATClient xnatClient;
    @Autowired
    private Reporter reporter;
    @Autowired
    private Path archiveRootPath;

    @Bean
    public CommandLineRunner run( ) {
        return args -> {

            String sessionLabel = null;
            String subjectLabel = null;
            String projectLabel = null;

            System.out.println("Parsing command line: " + Arrays.stream(args).collect( Collectors.joining(" ")));

            int iarg = 0;
            while( iarg < args.length) {
                switch (args[iarg].toLowerCase()) {
                    case "--sessionlabel":
                        sessionLabel = nextArg( args, ++iarg);
                        if( sessionLabel == null) {
                            String msg = "Missing sessionLabel argument for flag --sessionLabel";
                            System.out.println(msg);
                            System.exit(1);
                        }
                        iarg++;
                        break;
                    case "--subjectlabel":
                        subjectLabel = nextArg( args, ++iarg);
                        if( subjectLabel == null) {
                            String msg = "Missing subjectLabel argument for flag --subjectLabel";
                            System.out.println(msg);
                            System.exit(1);
                        }
                        iarg++;
                        break;
                    case "--projectlabel":
                        projectLabel = nextArg( args, ++iarg);
                        if( projectLabel == null) {
                            String msg = "Missing projectLabel argument for flag --projectLabel";
                            System.out.println(msg);
                            System.exit(1);
                        }
                        iarg++;
                        break;
                    default:
                        System.out.println("Unknown argument: " + args[iarg]);
                        printUsage();
                        System.exit(1);
                }
            }

            try {
                reporter.report( projectLabel, subjectLabel, System.out);

                log.info("Success.");
                System.exit(0);
            }
            catch (Exception e) {
                log.error("Failed.", e);
                System.exit(-1);
            }
        };
    }

    private String nextArg( String[] args, int iarg) {
        return (iarg < args.length)? args[iarg]: null;
    }

    private void printUsage() {
        System.out.println("dicomWeb-report --archiveRootPath <archiveRootPath> [--projectLabel <projectLabel> [--subjectLabel <subjectLabel> [--sessionLabel <sessionLabel>]]] ");
        System.out.println();
    }

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication( Application.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }

}