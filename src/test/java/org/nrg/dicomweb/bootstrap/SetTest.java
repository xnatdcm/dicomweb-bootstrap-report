package org.nrg.dicomweb.bootstrap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = { ApplicationTestConfig.class })
public class SetTest {
    private static String A = "A";
    private static String B = "B";
    private static String C = "C";
    private static String D = "D";

    @Test
    public void testSet() {
        String a1 = new String(A);
        String a2 = new String(A);
        String b1 = new String(B);
        String b2 = new String(B);
        String c1 = new String(C);
        String c2 = new String(C);
        String d1 = new String(D);
        String d2 = new String(D);

        List<String> l1 = Arrays.asList( a1, b1, c1);
        List<String> l2 = Arrays.asList( c1, b1, a1);
        List<String> l3 = Arrays.asList( a2, b2, c1);
        List<String> l4 = Arrays.asList( b1, c1);
        List<String> l5 = Arrays.asList( a1, d1);
        List<String> l6 = Arrays.asList( a2, d2);

        Set<String> sa1 = new HashSet<>(l1);
        Set<String> sa2 = new HashSet<>(l2);
        Set<String> sb1 = new HashSet<>(l3);
        Set<String> sb2 = new HashSet<>(l4);
        Set<String> sc1 = new HashSet<>(l5);
        Set<String> sc2 = new HashSet<>(l6);

        Set<Set<String>> set = new HashSet<>();

        set.add( sa1);
        set.add(sa2);
        set.add(sb1);
        set.add(sb2);
        set.add(sc1);
        set.add(sc2);

        List<String> names = Arrays.asList( "A", "B", "C", "D");

        Map<String, Set<String>> partition = partition(names, set);

        Set<String> Aset = new HashSet<>( Arrays.asList( B, C, D));
        Set<String> Bset = new HashSet<>( Arrays.asList( A, C));
        Set<String> Cset = new HashSet<>( Arrays.asList( A, B));
        Set<String> Dset = new HashSet<>( Arrays.asList( A));

        assertEquals(Aset, partition.get(A) );
        assertEquals(Bset, partition.get(B) );
        assertEquals(Cset, partition.get(C) );
        assertEquals(Dset, partition.get(D) );
    }

    public static Set<String> subset( String element, Set<Set<String>> setset) {
        Set<String> subset = new HashSet<>();
        setset.forEach( set -> {
            if( set.contains( element)) {
                subset.addAll( set);
                subset.remove( element);
            }
        });
        return subset;
    }

    public static Map<String, Set<String>> partition( List<String> elements, Set<Set<String>> setset) {
        return elements.stream()
                .collect(Collectors.toMap( e -> e, e -> subset( e, setset)));
    }

}