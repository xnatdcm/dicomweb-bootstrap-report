# DICOMweb-bootstrap-report #

This tool is an aide to bootstrap DICOMweb functionality into existing XNAT instances. This is a stand-alone application, meant to be
to be run once by an administrator to generate reports reflecting an XNAT instance's readiness for upgrading XNAT to 
a version that support DICOMweb. The generated reports provide information on to aide administrators in configuring the
upgrade process.

### Version ###

This code has been tested with the following versions.

Version        | Date | From XNAT version | To XNAT Version
---------------|------|------------|---------
1.0.0-SNAPSHOT | 2021-04-03 | 1.8.0, 1.8.1      | 1.8.2

### Usage ###

The jar file is executable.

    java -jar dicomweb-bootstrap-report.jar [--projectLabel <projectLabel>]

The default is to run on all projects in the archive. Execution can optionally be restricted to a single project by
supplying the project's label.

* This application extracts values from existing DICOM data in the archive. It must be run on a server that has
access to this filesystem.
  
This application provide the following reports
* Comparison of database contents with archived DICOM data.
* Duplicate study instance UIDs in multiple projects.
  
### Configuration ###

* All configuration is done in the file `application.properties` as part of the jar file. (***TODO***) Make this configuration
  more accessible to the user.
  * XNAT host coordinates and admin account credentials
  * XNAT database coordinates and admin account credentials

### Download ###

The jar file may be downloaded from the Download tab of this repository. (***TODO***)

### Build From Source ###

* Clone this repository.
* `mvn clean install`